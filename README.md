# Pipelines example

Pipeline CI/CD example

## Env variables

Add this env variables in bitbucket:

$FTP_USERNAME => FTP username

$FTP_PASSWORD => FTP password

$FTP_HOST => FTP host

$DOMAIN => Domain name

## Branches

dev/* => sync to folder `*-dev.$DOMAIN`

develop => sync to folder `dev.$DOMAIN`

staging => sync to folder `staging.$DOMAIN`

master => sync to folder `$DOMAIN`
